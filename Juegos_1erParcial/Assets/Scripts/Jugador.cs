﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Jugador : MonoBehaviour {

	private Rigidbody2D rb;
	public Text ScoreText;
	public float speed;
	private int contador;

	// Use this for initialization
	void Start () {

		rb = GetComponent<Rigidbody2D> ();
		contador = 0;
		SetScoreText ();
		
	}

	// Update is called once per frame
	void FixedUpdate () {

		float movimientoHorizontal = Input.GetAxis ("Horizontal");
		float movimientoVertical = Input.GetAxis ("Vertical");

		Vector2 movimiento = new Vector2 (movimientoHorizontal, movimientoVertical);

		rb.AddForce (movimiento * speed);

		
	}

	void OnTriggerEnter2D(Collider2D other){

		if (other.gameObject.CompareTag("Recolectables")) 
		{
			other.gameObject.SetActive (false);
			contador = contador + 1;
			SetScoreText ();

		}	
	}

	void SetScoreText (){
	
		ScoreText.text = "Score: " + contador.ToString ();

	}
}
