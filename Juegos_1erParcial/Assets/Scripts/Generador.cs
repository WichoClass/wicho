﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generador : MonoBehaviour {

	public GameObject prefab;

	void Start(){
	
		for(int i = 0; i <=9; i++){
			Vector2 position = new Vector2 (Random.Range (50f, 820f), Random.Range (45f, 370f));
			Instantiate (prefab, position, Quaternion.identity);
		}
	}

}
